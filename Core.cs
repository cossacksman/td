using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace tldr
{
    public class Core : Microsoft.Xna.Framework.Game
    {
        public static GraphicsDeviceManager Graphics;
        public static SpriteBatch SpriteBatch;
        public static SpriteFont DebugFont;
        public static string DebugString;

        int _TotalFrames = 0;
        int _Fps;
        TimeSpan _MillisecondsPerFrame = TimeSpan.FromMilliseconds(1000);
        TimeSpan _TimeSinceLastUpdate;

        public static int windowHeight;
        public static int windowWidth;
        public static int bufferHeight;
        public static int bufferWidth;

        public static KeyboardState previousKeyboardState;
        public static KeyboardState currentKeyboardState;

        public static QuadTree quadTree;
        public static PhysicsEngine physicsEngine = new PhysicsEngine();
        
        public Camera Camera;

        public static List<Entity> AllEntities = new List<Entity>();
        public static List<PhysicalEntity> PhysicalEntities = new List<PhysicalEntity>();
        public static List<LivingEntity> LivingEntities = new List<LivingEntity>();

        LivingEntity _player;

        public Core()
        {
            Graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";            
        }

        /// <summary>
        /// Initialize all game dependencies and core mechanics.
        /// </summary>
        protected override void Initialize()
        {
            base.Initialize();
            
            windowHeight = Graphics.GraphicsDevice.Adapter.CurrentDisplayMode.Height;
            windowWidth = Graphics.GraphicsDevice.Adapter.CurrentDisplayMode.Width;
            bufferHeight = Graphics.PreferredBackBufferHeight = windowHeight;
            bufferWidth = Graphics.PreferredBackBufferWidth = windowWidth;

            base.IsFixedTimeStep = true;
            base.IsMouseVisible = false;
            Graphics.IsFullScreen = false;
            Graphics.ApplyChanges();

            Camera = new Camera(GraphicsDevice.Viewport);
            DebugGenerateEntities();
            quadTree = new QuadTree(0, new Rectangle(0, 0, bufferWidth, bufferHeight));
        }


        private void DebugGenerateEntities()
        {
            Random rand = new Random();
            PhysicalEntity entity;
            for (int i = 0; i < 50; i++)
            {
                entity = new PhysicalEntity(10, 10, new Vector2(rand.Next(0, bufferWidth - 50), rand.Next(0, bufferHeight - 50)), Color.Purple, true);
            }
            _player = new LivingEntity(25, 25, new Vector2(1000f, 1000f), Color.White, true);
        }


        /// <summary>
        /// Load content upon initialization.
        /// </summary>
        protected override void LoadContent()
        {
            SpriteBatch = new SpriteBatch(GraphicsDevice);
            DebugFont = Content.Load<SpriteFont>("Fonts/DebugFont");            
        }

        /// <summary>
        /// Unload any unused or disposed content.
        /// </summary>
        protected override void UnloadContent()
        {
        }

        /// <summary>
        /// Handle all game updating, calling Update methods
        /// from all existing updatables conditionally.
        /// </summary>
        protected override void Update(GameTime gameTime)
        {
            _TimeSinceLastUpdate += gameTime.ElapsedGameTime;
            if (_TimeSinceLastUpdate >= _MillisecondsPerFrame)
            {
                _TimeSinceLastUpdate -= _MillisecondsPerFrame;
                _Fps = _TotalFrames;
                _TotalFrames = 0;
            }
            
            UpdateInput();

            // Update all entities
            foreach (Entity entity in AllEntities)
                entity.Update(gameTime, currentKeyboardState);

            Camera.Update(gameTime, _player);
            physicsEngine.Update(gameTime);

            quadTree.Update(gameTime);
            
            base.Update(gameTime);
        }

        /// <summary>
        /// Set keyboard states and control input.
        /// Not for input of controllables.
        /// </summary>
        private void UpdateInput()
        {
            currentKeyboardState = Keyboard.GetState();

            // Close application upon 360 "Back" or Keyboard "Escape"
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || currentKeyboardState.IsKeyDown(Keys.Escape))
                this.Exit();

            previousKeyboardState = currentKeyboardState;
        }

        /// <summary>
        /// Handle all game drawing, calling Draw methods from
        /// all existing drawables conditionally.
        /// </summary>
        protected override void Draw(GameTime gameTime)
        {
            _TotalFrames++;
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // DYNAMIC DRAWING
            SpriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, null, null, null, null, Camera.transform);
            foreach(Entity ent in AllEntities)
                if(ent.IsDrawn) { ent.Draw(SpriteBatch, gameTime); }
            quadTree.Draw(SpriteBatch);
            SpriteBatch.DrawString(DebugFont, quadTree.debugString, new Vector2(5, 25), Color.White);
            SpriteBatch.End();

            // STATIC DRAWING
            SpriteBatch.Begin();
            SpriteBatch.DrawString(DebugFont, "FPS: " +_Fps.ToString(), new Vector2(5, 5), Color.White);
            SpriteBatch.End();

            base.Draw(gameTime);
        }
    }
}