﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace tldr
{
    public abstract class Entity
    {
        public Texture2D Texture;
        public Color Color;
        public int Height;
        public int Width;

        public Vector2 Position;
        public Vector2 Velocity;
        public Vector2 MaxVelocity;
        public float Mass;
        
        public Rectangle BoundingBox;
        
        public bool HasGravity;
        public bool IsDrawn = true;
        public bool IsMovable;

        public string debugString = "";

        public abstract void Update(GameTime gameTime, KeyboardState keyboardState);

        /// <summary>
        /// Drawing handled by the inheritable Entity class
        /// for all entities derived from this.
        /// </summary>
        public void Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            spriteBatch.Draw(this.Texture, this.Position, this.Color);
            Core.SpriteBatch.DrawString(Core.DebugFont, this.debugString, new Vector2(this.Position.X - 100f, this.Position.Y - 100f), Color.Black);
        }
    }
}