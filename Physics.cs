﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace tldr
{
    public class PhysicsEngine
    {
        // Define an event based on the delegate
        public event EventHandler<PhysicsEventArgs> CollisionDetected;

        public String debugString = "";

        public class PhysicsEventArgs : EventArgs
        {
            public Entity perspectiveEntity { get; set; }
            public Entity collidedEntity { get; set; }
        }

        /// <summary>
        /// Apply the velocity of an object
        /// to it's  own position fields.
        /// </summary>
        public void Velocity()
        {
            foreach (Entity entity in Core.AllEntities)
            {
                entity.Position += entity.Velocity;
                if (entity.Velocity.X >= entity.MaxVelocity.X) { entity.Velocity.X = entity.MaxVelocity.X; }
                if (entity.Velocity.Y >= entity.MaxVelocity.Y) { entity.Velocity.Y = entity.MaxVelocity.Y; }
                if (entity.Velocity.X <= -entity.MaxVelocity.X) { entity.Velocity.X = -entity.MaxVelocity.X; }
                if (entity.Velocity.Y <= -entity.MaxVelocity.Y) { entity.Velocity.Y = -entity.MaxVelocity.Y; }
            }
        }
        
        /// <summary>
        /// Detect collisions within the NodeTree.
        /// </summary>

        public void Collision(List<Entity> objects)
        {
            // Loop through objects within current node in quad tree
            // to define the comparer
            for (int i = 0; i < objects.Count(); i++)
            {
                Entity _currentEntity = objects[i];
                // loop through possible collidables in the current node
                // of the quad tree
                for (int l = 0; l < objects.Count(); l++)
                {
                    Entity _collidableEntity = objects[l];
                    if (_currentEntity.BoundingBox.Intersects(_collidableEntity.BoundingBox))
                    {
                        OnCollision(_currentEntity, _collidableEntity);
                    }
                }
            }
        }

        /// <summary>
        /// Apply gravity to entity positional fields
        /// </summary>
        public void Gravity(GameTime gameTime)
        {
            // Apply gravity to entities affected by gravity.
            foreach (Entity entity in Core.AllEntities)
            {
                // Apply gravity
            }
        }

        /// <summary>
        /// Apply Friction to entity positional fields
        /// </summary>
        public void Friction()
        {
            foreach (Entity entity in Core.AllEntities)
            {
                if (entity.Velocity.X > 0)
                    entity.Velocity.X -= 0.5f;
                if (entity.Velocity.Y > 0)
                    entity.Velocity.Y -= 0.5f;
                if (entity.Velocity.X < 0)
                    entity.Velocity.X += 0.5f;
                if (entity.Velocity.Y < 0)
                    entity.Velocity.Y += 0.5f;
            }
        }

        /// <summary>
        /// Update the physics engine, detecting 
        /// collisions and updating fields
        /// </summary>
        public void Update(GameTime gameTime)
        {
            this.Velocity();
            this.Friction();
            // this.Gravity(gameTime);
        }
        

          ///////////////////////////
         // Event virtual methods //
        ///////////////////////////

        /// <summary>
        /// Collision event
        /// </summary>
        protected virtual void OnCollision(Entity entityPerspective, Entity entityCollided)
        {
            if (CollisionDetected != null)
            {
                CollisionDetected(this, new PhysicsEventArgs(){ 
                    perspectiveEntity = entityPerspective,
                    collidedEntity = entityCollided
                });
            }
        }
    }
}
