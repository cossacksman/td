﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace tldr
{
    public class QuadTree
    {
        private int _MaxObjects = 2;
        private int _MaxLevels = 10;

        private int _Level;
        private List<Entity> _Objects;
        private Rectangle _Bounds;
        private QuadTree[] _Nodes;

        private Texture2D _Pixel;

        public string debugString = "";
        
        public QuadTree(int pLevel, Rectangle bounds)
        {
            _Level = pLevel;
            _Objects = new List<Entity>();
            _Bounds = bounds;
            _Nodes = new QuadTree[4];
            Init();
        }

        private void Init()
        {
            _Pixel = new Texture2D(Core.Graphics.GraphicsDevice, 1, 1, false, SurfaceFormat.Color);
            _Pixel.SetData(new[] { Color.White});
        }

        /// <summary>
        /// Clears the master node and all child nodes within the quad tree
        /// </summary>
        public void Clear()
        {
            // Empty the used list of objects
            _Objects.Clear();

            // Remove all objects from quadtree
            for (int i = 0; i < _Nodes.Length; i++)
            {
                if (_Nodes[i] != null)
                {
                    Array.Clear(_Nodes, 0, _Nodes.Length);
                    _Nodes[i] = null;
                }
            }
        }

        /// <summary>
        /// Splits a given node in the quadtree into four child nodes
        /// </summary>
        public void Split()
        {
            // Use the properties passed to the node for variables
            int subWidth = (int)(_Bounds.Width / 2);
            int subHeight = (int)(_Bounds.Height / 2);
            int x = (int)_Bounds.X;
            int y = (int)_Bounds.Y;

            // Create four new nodes from the new variables
            _Nodes[0] = new QuadTree(_Level + 1, new Rectangle(x + subWidth, y, subWidth, subHeight));
            _Nodes[1] = new QuadTree(_Level + 1, new Rectangle(x, y, subWidth, subHeight));
            _Nodes[2] = new QuadTree(_Level + 1, new Rectangle(x, y + subHeight, subWidth, subHeight));
            _Nodes[3] = new QuadTree(_Level + 1, new Rectangle(x + subWidth, y + subHeight, subWidth, subHeight));
        }

        /// <summary>
        /// Get the node index of the object in question.
        /// -1 is an object in multiple nodes.
        /// </summary>
        private int GetIndex(Entity entity)
        {
            int Index = -1;
            double VerticalMidpoint = _Bounds.X + (_Bounds.Width / 2);
            double HorizontalMidpoint = _Bounds.Y + (_Bounds.Height / 2);

            // Conditional boolean values to use with the left/right quadran checks
            bool TopQuadrant = (entity.BoundingBox.Y < HorizontalMidpoint && entity.BoundingBox.Y + entity.BoundingBox.Height < HorizontalMidpoint);
            bool BottomQuadrant = (entity.BoundingBox.Y > HorizontalMidpoint);

            // Left quad
            if (entity.BoundingBox.X < VerticalMidpoint && entity.BoundingBox.X + entity.Width < VerticalMidpoint)
            {
                if (TopQuadrant)
                    Index = 1;
                else if (BottomQuadrant)
                    Index = 2;
            }
            // Right quad
            if (entity.BoundingBox.X > VerticalMidpoint)
            {
                if (TopQuadrant)
                    Index = 0;
                else if (BottomQuadrant)
                    Index = 3;
            }

            return Index;
        }

        /// <summary>
        /// Inserts an object into the quad tree, splitting the quads
        /// if the nodes exceed the maximum quantity.
        /// </summary>
        public void Insert(Entity entity)
        {
            // Try to add the object to the current quad tree 
            if (_Nodes[0] != null)
            {
                int Index = GetIndex(entity);
                if (Index != -1)
                {
                    _Nodes[Index].Insert(entity);
                    return;
                }
            }
            
            _Objects.Add(entity);

            // If the maxmimum levels have not been reached but the maximum amount of 
            // objects in a quad have been, split and compare
            if (_Objects.Count() > _MaxObjects && _Level < _MaxLevels)
            {
                // If the next quad tree does not yet exist, create it (subdivide)
                if (_Nodes[0] == null)
                    Split();

                int i = 0;
                while (i < _Objects.Count)
                {
                    int Index = GetIndex(_Objects[i]);
                    if (Index != -1)
                    {
                        _Nodes[Index].Insert(_Objects[i]);
                        _Objects.Remove(_Objects[i]);
                    }
                    else
                    {
                        i++;
                    }
                }
            }
        }

        /// <summary>
        /// Retrieve all collidables from the quad tree around a rectangle
        /// </summary>
        public List<Entity> Retrieve(List<Entity> returnobjects, Entity entity)
        {            
            int index = GetIndex(entity);
            List<Entity> nodEnts = new List<Entity>();

            if (index != -1 && _Nodes[0] != null)
                _Nodes[index].Retrieve(returnobjects, entity);

            returnobjects.AddRange(_Objects);

            return returnobjects;
        }
        
        /// <summary>
        /// Update the quad tree in game time
        /// </summary>
        public void Update(GameTime gameTime)
        {
            // Clear and insert objects into the quad tree
            Clear();

            for (int i = 0; i < Core.AllEntities.Count; i++)
                Insert(Core.AllEntities[i]);

            // Check the objects in the quads and loop collidables
            List<Entity> returnObjects = new List<Entity>();
            for (int i = 0; i < _Objects.Count; i++)
            {
                returnObjects.Clear();
                Retrieve(returnObjects, _Objects[i]);

                for (int x = 0; x < returnObjects.Count(); x++)
                    debugString = returnObjects.Count.ToString();
                    //Core.physicsEngine.Collision(returnObjects);
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            if (_Nodes[0] != null)
            {
                // Set debugstring
            }

            for (int a = 0; a < _Nodes.Length; a++)
            {
                DrawBorder(_Bounds, 1, Color.Green, spriteBatch);

                if (_Nodes[a] != null)
                {
                    DrawBorder(_Nodes[a]._Bounds, 1, Color.Green, spriteBatch);
                    if (_Nodes[a]._Nodes[0] != null)
                    {
                        for (int b = 0; b < _Nodes.Length; b++)
                        {
                            DrawBorder(_Nodes[a]._Nodes[b]._Bounds, 1, Color.Green, spriteBatch);
                            if (_Nodes[a]._Nodes[b]._Nodes[0] != null)
                            {
                                for (int c = 0; c < _Nodes.Length; c++)
                                {
                                    DrawBorder(_Nodes[a]._Nodes[b]._Nodes[c]._Bounds, 1, Color.Green, spriteBatch);
                                    if (_Nodes[a]._Nodes[b]._Nodes[c]._Nodes[0] != null)
                                    {
                                        for (int d = 0; d < _Nodes.Length; d++)
                                        {
                                            DrawBorder(_Nodes[a]._Nodes[b]._Nodes[c]._Nodes[d]._Bounds, 1, Color.Green, spriteBatch);
                                            if (_Nodes[a]._Nodes[b]._Nodes[c]._Nodes[d]._Nodes[0] != null)
                                            {
                                                for (int e = 0; e < _Nodes.Length; e++)
                                                {
                                                    DrawBorder(_Nodes[a]._Nodes[b]._Nodes[c]._Nodes[d]._Nodes[e]._Bounds, 1, Color.Green, spriteBatch);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        private void DrawBorder(Rectangle rectangleToDraw, int thicknessOfBorder, Color borderColor, SpriteBatch spriteBatch)
        {
            // Draw top line
            spriteBatch.Draw(_Pixel, new Rectangle(rectangleToDraw.X, rectangleToDraw.Y, rectangleToDraw.Width, thicknessOfBorder), borderColor);

            // Draw left line
            spriteBatch.Draw(_Pixel, new Rectangle(rectangleToDraw.X, rectangleToDraw.Y, thicknessOfBorder, rectangleToDraw.Height), borderColor);

            // Draw right line
            spriteBatch.Draw(_Pixel, new Rectangle((rectangleToDraw.X + rectangleToDraw.Width - thicknessOfBorder),
                                            rectangleToDraw.Y,
                                            thicknessOfBorder,
                                            rectangleToDraw.Height), borderColor);
            // Draw bottom line
            spriteBatch.Draw(_Pixel, new Rectangle(rectangleToDraw.X,
                                            rectangleToDraw.Y + rectangleToDraw.Height - thicknessOfBorder,
                                            rectangleToDraw.Width,
                                            thicknessOfBorder), borderColor);
        }
    }
}