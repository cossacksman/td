﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace tldr
{
    public class LivingEntity : Entity
    {
        public int StepSize;

        public int Health;
        public int Range;

        public bool IsControllable;
        
        public LivingEntity(int height, int width, Vector2 position, Color color, bool controllable)
        {
            this.MaxVelocity = new Vector2(4f,4f);
            this.Position = position;
            this.StepSize = 2;
            this.Height = height;
            this.Width = width;
            this.Color = color;
            this.BoundingBox = new Rectangle((int)this.Position.X, (int)this.Position.Y, this.Width, this.Height);
            this.IsControllable = controllable;
            this.Texture = new Texture2D(Core.Graphics.GraphicsDevice, this.Width, this.Height, false, SurfaceFormat.Color);
            Color[] colors = Enumerable.Range(0, this.Height * this.Width).Select(i => this.Color).ToArray();
            this.Texture.SetData(colors);
            this.Init();
        }

        /// <summary>
        /// Initialize anything before adding to the the core 
        /// entity generic collections.
        /// </summary>
        private void Init()
        {
            Core.AllEntities.Add(this);
            Core.LivingEntities.Add(this);
            Core.physicsEngine.CollisionDetected += this.OnCollision;
        }

        /// <summary>
        /// Uses the current keyboard state to interact with the
        /// entity, passed by the update method of this object.
        /// </summary>
        public void Input(KeyboardState keyboardState)
        {
            if (keyboardState.IsKeyDown(Keys.W))
                this.Velocity.Y -= this.StepSize;
            if (keyboardState.IsKeyDown(Keys.A))
                this.Velocity.X -= this.StepSize;
            if (keyboardState.IsKeyDown(Keys.S))
                this.Velocity.Y += this.StepSize;
            if (keyboardState.IsKeyDown(Keys.D))
                this.Velocity.X += this.StepSize;
        }

        /// <summary>
        /// Collects a single keyboard state and provides a means to
        /// update any fields or methods with potential to change.
        /// </summary>
        public override void Update(GameTime gameTime, KeyboardState keyboardState)
        {
            this.BoundingBox.X = (int)this.Position.X;
            this.BoundingBox.Y = (int)this.Position.Y;

            if (this.IsControllable)
                this.Input(keyboardState);
        }

        // Event methods
        public void OnCollision(object source, PhysicsEngine.PhysicsEventArgs e)
        {
            if (e.perspectiveEntity == this)
            {
                debugString = "Collided with object at X: " + e.collidedEntity.Position.X + " and Y: " + e.collidedEntity.Position.Y;
            }
        }
    }
}