﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace tldr
{
    public class PhysicalEntity : Entity
    {
        public bool IsBreakable;
        public bool IsSolid;
                
        public PhysicalEntity(int height, int width, Vector2 position, Color color, bool solid)
        {
            this.Height = height;
            this.Width = width;
            this.Position = position;
            this.Color = color;
            this.IsSolid = solid;
            this.BoundingBox = new Rectangle((int)this.Position.X, (int)this.Position.Y, this.Width, this.Height);
            this.Texture = new Texture2D(Core.Graphics.GraphicsDevice, this.Width, this.Height, false, SurfaceFormat.Color);
            Color[] colors = Enumerable.Range(0, this.Height*this.Width).Select(i => this.Color).ToArray();
            this.Texture.SetData(colors);
            this.Init();
        }

        /// <summary>
        /// Initialize anything before adding to the the core 
        /// entity generic collections.
        /// </summary>
        public void Init()
        {
            Core.AllEntities.Add(this);
            Core.PhysicalEntities.Add(this);
        }

        /// <summary>
        /// Update any fields or methods with potential to change.
        /// </summary>
        public override void Update(GameTime gameTime, KeyboardState keyboardState)
        {
            this.BoundingBox.X = (int)this.Position.X;
            this.BoundingBox.Y = (int)this.Position.Y;
        }

    }
}